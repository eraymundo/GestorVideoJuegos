﻿insert into Carreras (carrera) values ('Perito en Electrónica de Computación')
insert into Carreras (carrera) values ('Perito en Electrónica Industrial')
insert into Carreras (carrera) values ('Perito en Electricidad Industrial')
insert into Carreras (carrera) values ('Perito en Dibujo Técnico de Ingeniería y Arquitectura')
insert into Carreras (carrera) values ('Perito en Mecánica Automotriz')

insert into Grados (grado) values ('Cuarto')
insert into Grados (grado) values ('Quinto')
insert into Grados (grado) values ('Sexto')
insert into Grados (grado) values ('Primero Basico')
insert into Grados (grado) values ('Segundo Basico')
insert into Grados (grado) values ('Tercero Basico')

insert into Seccions (seccion) values ('A')
insert into Seccions (seccion) values ('B')

insert into Jornadas (jornada) values ('Matutina')
insert into Jornadas (jornada) values ('Vespertina')

insert into numeroParticipantes (numero) values (32)

insert into Torneos (nombre, numeroDeParticipantes) values ('FIFA', 1)
insert into Torneos (nombre, numeroDeParticipantes) values ('SMASH', 1)

insert into GenerarCodigoes (codigo, codigoTorneo) values ('AF32KJ4', 3)

insert into Gamers (Nombres, Apellidos, Carnet, codigo, idCarrera, idGrado, idSeccion, idJornada) 
values ('Elio', 'Raymundo', '2016076', 4,1,1,1,1)
