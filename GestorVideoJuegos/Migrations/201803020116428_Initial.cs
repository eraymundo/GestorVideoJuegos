namespace GestorVideoJuegos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Carreras",
                c => new
                    {
                        idCarrera = c.Int(nullable: false, identity: true),
                        carrera = c.String(),
                    })
                .PrimaryKey(t => t.idCarrera);
            
            CreateTable(
                "dbo.Gamers",
                c => new
                    {
                        idGamer = c.Int(nullable: false, identity: true),
                        Nombres = c.String(),
                        Apellidos = c.String(),
                        Carnet = c.String(),
                        codigo = c.Int(nullable: false),
                        idCarrera = c.Int(nullable: false),
                        idGrado = c.Int(nullable: false),
                        idSeccion = c.Int(nullable: false),
                        idJornada = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idGamer)
                .ForeignKey("dbo.Carreras", t => t.idCarrera, cascadeDelete: true)
                .ForeignKey("dbo.GenerarCodigoes", t => t.codigo, cascadeDelete: true)
                .ForeignKey("dbo.Grados", t => t.idGrado, cascadeDelete: true)
                .ForeignKey("dbo.Jornadas", t => t.idJornada, cascadeDelete: true)
                .ForeignKey("dbo.Seccions", t => t.idSeccion, cascadeDelete: true)
                .Index(t => t.idCarrera)
                .Index(t => t.codigo)
                .Index(t => t.idGrado)
                .Index(t => t.idJornada)
                .Index(t => t.idSeccion);
            
            CreateTable(
                "dbo.GenerarCodigoes",
                c => new
                    {
                        idGenerarCodigo = c.Int(nullable: false, identity: true),
                        codigo = c.String(),
                        codigoTorneo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idGenerarCodigo)
                .ForeignKey("dbo.Torneos", t => t.codigoTorneo, cascadeDelete: true)
                .Index(t => t.codigoTorneo);
            
            CreateTable(
                "dbo.Torneos",
                c => new
                    {
                        idTorneo = c.Int(nullable: false, identity: true),
                        nombre = c.String(),
                        numeroDeParticipantes = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idTorneo)
                .ForeignKey("dbo.NumeroParticipantes", t => t.numeroDeParticipantes, cascadeDelete: true)
                .Index(t => t.numeroDeParticipantes);
            
            CreateTable(
                "dbo.NumeroParticipantes",
                c => new
                    {
                        idNumeroParticipantes = c.Int(nullable: false, identity: true),
                        numero = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idNumeroParticipantes);
            
            CreateTable(
                "dbo.Grados",
                c => new
                    {
                        idGrado = c.Int(nullable: false, identity: true),
                        grado = c.String(),
                    })
                .PrimaryKey(t => t.idGrado);
            
            CreateTable(
                "dbo.Jornadas",
                c => new
                    {
                        idJornada = c.Int(nullable: false, identity: true),
                        jornada = c.String(),
                    })
                .PrimaryKey(t => t.idJornada);
            
            CreateTable(
                "dbo.Seccions",
                c => new
                    {
                        idSeccion = c.Int(nullable: false, identity: true),
                        seccion = c.String(),
                    })
                .PrimaryKey(t => t.idSeccion);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        UserName = c.String(),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        carrera_idCarrera = c.Int(),
                        gamer_idGamer = c.Int(),
                        generarCodigo_idGenerarCodigo = c.Int(),
                        grados_idGrado = c.Int(),
                        jornada_idJornada = c.Int(),
                        participantes_idNumeroParticipantes = c.Int(),
                        seccion_idSeccion = c.Int(),
                        torneos_idTorneo = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Carreras", t => t.carrera_idCarrera)
                .ForeignKey("dbo.Gamers", t => t.gamer_idGamer)
                .ForeignKey("dbo.GenerarCodigoes", t => t.generarCodigo_idGenerarCodigo)
                .ForeignKey("dbo.Grados", t => t.grados_idGrado)
                .ForeignKey("dbo.Jornadas", t => t.jornada_idJornada)
                .ForeignKey("dbo.NumeroParticipantes", t => t.participantes_idNumeroParticipantes)
                .ForeignKey("dbo.Seccions", t => t.seccion_idSeccion)
                .ForeignKey("dbo.Torneos", t => t.torneos_idTorneo)
                .Index(t => t.carrera_idCarrera)
                .Index(t => t.gamer_idGamer)
                .Index(t => t.generarCodigo_idGenerarCodigo)
                .Index(t => t.grados_idGrado)
                .Index(t => t.jornada_idJornada)
                .Index(t => t.participantes_idNumeroParticipantes)
                .Index(t => t.seccion_idSeccion)
                .Index(t => t.torneos_idTorneo);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        User_Id = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id, cascadeDelete: true)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.LoginProvider, t.ProviderKey })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "torneos_idTorneo", "dbo.Torneos");
            DropForeignKey("dbo.AspNetUsers", "seccion_idSeccion", "dbo.Seccions");
            DropForeignKey("dbo.AspNetUsers", "participantes_idNumeroParticipantes", "dbo.NumeroParticipantes");
            DropForeignKey("dbo.AspNetUsers", "jornada_idJornada", "dbo.Jornadas");
            DropForeignKey("dbo.AspNetUsers", "grados_idGrado", "dbo.Grados");
            DropForeignKey("dbo.AspNetUsers", "generarCodigo_idGenerarCodigo", "dbo.GenerarCodigoes");
            DropForeignKey("dbo.AspNetUsers", "gamer_idGamer", "dbo.Gamers");
            DropForeignKey("dbo.AspNetUserClaims", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "carrera_idCarrera", "dbo.Carreras");
            DropForeignKey("dbo.Gamers", "idSeccion", "dbo.Seccions");
            DropForeignKey("dbo.Gamers", "idJornada", "dbo.Jornadas");
            DropForeignKey("dbo.Gamers", "idGrado", "dbo.Grados");
            DropForeignKey("dbo.Gamers", "codigo", "dbo.GenerarCodigoes");
            DropForeignKey("dbo.GenerarCodigoes", "codigoTorneo", "dbo.Torneos");
            DropForeignKey("dbo.Torneos", "numeroDeParticipantes", "dbo.NumeroParticipantes");
            DropForeignKey("dbo.Gamers", "idCarrera", "dbo.Carreras");
            DropIndex("dbo.AspNetUsers", new[] { "torneos_idTorneo" });
            DropIndex("dbo.AspNetUsers", new[] { "seccion_idSeccion" });
            DropIndex("dbo.AspNetUsers", new[] { "participantes_idNumeroParticipantes" });
            DropIndex("dbo.AspNetUsers", new[] { "jornada_idJornada" });
            DropIndex("dbo.AspNetUsers", new[] { "grados_idGrado" });
            DropIndex("dbo.AspNetUsers", new[] { "generarCodigo_idGenerarCodigo" });
            DropIndex("dbo.AspNetUsers", new[] { "gamer_idGamer" });
            DropIndex("dbo.AspNetUserClaims", new[] { "User_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "carrera_idCarrera" });
            DropIndex("dbo.Gamers", new[] { "idSeccion" });
            DropIndex("dbo.Gamers", new[] { "idJornada" });
            DropIndex("dbo.Gamers", new[] { "idGrado" });
            DropIndex("dbo.Gamers", new[] { "codigo" });
            DropIndex("dbo.GenerarCodigoes", new[] { "codigoTorneo" });
            DropIndex("dbo.Torneos", new[] { "numeroDeParticipantes" });
            DropIndex("dbo.Gamers", new[] { "idCarrera" });
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Seccions");
            DropTable("dbo.Jornadas");
            DropTable("dbo.Grados");
            DropTable("dbo.NumeroParticipantes");
            DropTable("dbo.Torneos");
            DropTable("dbo.GenerarCodigoes");
            DropTable("dbo.Gamers");
            DropTable("dbo.Carreras");
        }
    }
}
