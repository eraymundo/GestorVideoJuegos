﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GestorVideoJuegos.Models
{
    public class Seccion
    {
        [Key]
        public int idSeccion { get; set; }

        public String seccion { get; set; }

    }
}