﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestorVideoJuegos.Models
{
    public class GenerarCodigo
    {
        [Key]
        public int idGenerarCodigo { get; set; }

        public String codigo { get; set; }

        [ForeignKey("torneo")]
        public int codigoTorneo { get; set; }
        public virtual Torneos torneo { get; set; }
    }
}