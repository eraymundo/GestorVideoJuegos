﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GestorVideoJuegos.Models
{
    public class Grados
    {
        [Key]
        public int idGrado { get; set; }

        public String grado { get; set; }

    }
}