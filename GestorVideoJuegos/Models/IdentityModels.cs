﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace GestorVideoJuegos.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public virtual NumeroParticipantes participantes { get; set; }

        public virtual Torneos torneos { get; set; }

        public virtual GenerarCodigo generarCodigo { get; set; }

        public virtual Gamer gamer { get; set; }

        public virtual Carrera carrera { get; set; }

        public virtual Grados grados { get; set; }

        public virtual Seccion seccion { get; set; }

        public virtual Jornada jornada { get; set; }


    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public System.Data.Entity.DbSet<GestorVideoJuegos.Models.Gamer> Gamers { get; set; }

        public System.Data.Entity.DbSet<GestorVideoJuegos.Models.Carrera> Carreras { get; set; }

        public System.Data.Entity.DbSet<GestorVideoJuegos.Models.GenerarCodigo> GenerarCodigoes { get; set; }

        public System.Data.Entity.DbSet<GestorVideoJuegos.Models.Grados> Grados { get; set; }

        public System.Data.Entity.DbSet<GestorVideoJuegos.Models.Jornada> Jornadas { get; set; }

        public System.Data.Entity.DbSet<GestorVideoJuegos.Models.Seccion> Seccions { get; set; }
    }
}