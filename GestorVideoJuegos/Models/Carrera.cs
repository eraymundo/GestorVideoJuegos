﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GestorVideoJuegos.Models
{
    public class Carrera
    {
        [Key]
        public int idCarrera { get; set; }

        public String carrera { get; set; }

    }
}