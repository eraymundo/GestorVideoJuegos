﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GestorVideoJuegos.Models
{
    public class Jornada
    {
        [Key]
        public int idJornada { get; set; }

        public String jornada { get; set; }

    }
}