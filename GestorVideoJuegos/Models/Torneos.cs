﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestorVideoJuegos.Models
{
    public class Torneos
    {
        [Key]
        public int idTorneo { get; set; }

        public String nombre { get; set; }

        [ForeignKey("numero")]
        public int numeroDeParticipantes { get; set; }
        public virtual NumeroParticipantes numero { get; set; }
    }
}