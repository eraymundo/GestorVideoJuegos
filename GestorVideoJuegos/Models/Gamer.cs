﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GestorVideoJuegos.Models
{
    public class Gamer
    {
        [Key]
        public int idGamer { get; set; }

        public String Nombres { get; set; }

        public String Apellidos { get; set; }

        public String Carnet { get; set; }

        [ForeignKey("code")]
        public int codigo { get; set; }
        public virtual GenerarCodigo code { get; set; }


        [ForeignKey("carrera")]
        public int idCarrera { get; set; }
        public virtual Carrera carrera { get; set; }


        [ForeignKey("grado")]
        public int idGrado { get; set; }
        public virtual Grados grado { get; set; }


        [ForeignKey("seccion")]
        public int idSeccion { get; set; }
        public virtual Seccion seccion { get; set; }


        [ForeignKey("jornada")]
        public int idJornada { get; set; }
        public virtual Jornada jornada { get; set; }


    }
}