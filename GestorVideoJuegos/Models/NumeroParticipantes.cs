﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GestorVideoJuegos.Models
{
    public class NumeroParticipantes
    {
        [Key]
        public int idNumeroParticipantes { get; set; }

        public int numero { get; set; }

    }
}