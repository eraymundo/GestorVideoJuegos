﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GestorVideoJuegos.Startup))]
namespace GestorVideoJuegos
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
