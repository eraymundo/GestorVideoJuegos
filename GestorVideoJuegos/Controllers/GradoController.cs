﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestorVideoJuegos.Models;

namespace GestorVideoJuegos.Controllers
{
    public class GradoController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Grado/
        public async Task<ActionResult> Index()
        {
            return View(await db.Grados.ToListAsync());
        }

        // GET: /Grado/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grados grados = await db.Grados.FindAsync(id);
            if (grados == null)
            {
                return HttpNotFound();
            }
            return View(grados);
        }

        // GET: /Grado/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Grado/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include="idGrado,grado")] Grados grados)
        {
            if (ModelState.IsValid)
            {
                db.Grados.Add(grados);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(grados);
        }

        // GET: /Grado/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grados grados = await db.Grados.FindAsync(id);
            if (grados == null)
            {
                return HttpNotFound();
            }
            return View(grados);
        }

        // POST: /Grado/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include="idGrado,grado")] Grados grados)
        {
            if (ModelState.IsValid)
            {
                db.Entry(grados).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(grados);
        }

        // GET: /Grado/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grados grados = await db.Grados.FindAsync(id);
            if (grados == null)
            {
                return HttpNotFound();
            }
            return View(grados);
        }

        // POST: /Grado/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Grados grados = await db.Grados.FindAsync(id);
            db.Grados.Remove(grados);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
