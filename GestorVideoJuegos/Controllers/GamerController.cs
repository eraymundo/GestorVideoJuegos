﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GestorVideoJuegos.Models;

namespace GestorVideoJuegos.Controllers
{
    public class GamerController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: /Gamer/
        public async Task<ActionResult> Index()
        {
            var gamers = db.Gamers.Include(g => g.carrera).Include(g => g.code).Include(g => g.grado).Include(g => g.jornada).Include(g => g.seccion);
            return View(await gamers.ToListAsync());
        }

        // GET: /Gamer/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gamer gamer = await db.Gamers.FindAsync(id);
            if (gamer == null)
            {
                return HttpNotFound();
            }
            return View(gamer);
        }

        // GET: /Gamer/Create
        public ActionResult Create()
        {
            ViewBag.idCarrera = new SelectList(db.Carreras, "idCarrera", "carrera");
            ViewBag.codigo = new SelectList(db.GenerarCodigoes, "idGenerarCodigo", "codigo");
            ViewBag.idGrado = new SelectList(db.Grados, "idGrado", "grado");
            ViewBag.idJornada = new SelectList(db.Jornadas, "idJornada", "jornada");
            ViewBag.idSeccion = new SelectList(db.Seccions, "idSeccion", "seccion");
            return View();
        }

        // POST: /Gamer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include="idGamer,Nombres,Apellidos,Carnet,codigo,idCarrera,idGrado,idSeccion,idJornada")] Gamer gamer)
        {
            if (ModelState.IsValid)
            {
                db.Gamers.Add(gamer);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idCarrera = new SelectList(db.Carreras, "idCarrera", "carrera", gamer.idCarrera);
            ViewBag.codigo = new SelectList(db.GenerarCodigoes, "idGenerarCodigo", "codigo", gamer.codigo);
            ViewBag.idGrado = new SelectList(db.Grados, "idGrado", "grado", gamer.idGrado);
            ViewBag.idJornada = new SelectList(db.Jornadas, "idJornada", "jornada", gamer.idJornada);
            ViewBag.idSeccion = new SelectList(db.Seccions, "idSeccion", "seccion", gamer.idSeccion);
            return View(gamer);
        }

        // GET: /Gamer/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gamer gamer = await db.Gamers.FindAsync(id);
            if (gamer == null)
            {
                return HttpNotFound();
            }
            ViewBag.idCarrera = new SelectList(db.Carreras, "idCarrera", "carrera", gamer.idCarrera);
            ViewBag.codigo = new SelectList(db.GenerarCodigoes, "idGenerarCodigo", "codigo", gamer.codigo);
            ViewBag.idGrado = new SelectList(db.Grados, "idGrado", "grado", gamer.idGrado);
            ViewBag.idJornada = new SelectList(db.Jornadas, "idJornada", "jornada", gamer.idJornada);
            ViewBag.idSeccion = new SelectList(db.Seccions, "idSeccion", "seccion", gamer.idSeccion);
            return View(gamer);
        }

        // POST: /Gamer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include="idGamer,Nombres,Apellidos,Carnet,codigo,idCarrera,idGrado,idSeccion,idJornada")] Gamer gamer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gamer).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idCarrera = new SelectList(db.Carreras, "idCarrera", "carrera", gamer.idCarrera);
            ViewBag.codigo = new SelectList(db.GenerarCodigoes, "idGenerarCodigo", "codigo", gamer.codigo);
            ViewBag.idGrado = new SelectList(db.Grados, "idGrado", "grado", gamer.idGrado);
            ViewBag.idJornada = new SelectList(db.Jornadas, "idJornada", "jornada", gamer.idJornada);
            ViewBag.idSeccion = new SelectList(db.Seccions, "idSeccion", "seccion", gamer.idSeccion);
            return View(gamer);
        }

        // GET: /Gamer/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gamer gamer = await db.Gamers.FindAsync(id);
            if (gamer == null)
            {
                return HttpNotFound();
            }
            return View(gamer);
        }

        // POST: /Gamer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Gamer gamer = await db.Gamers.FindAsync(id);
            db.Gamers.Remove(gamer);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
